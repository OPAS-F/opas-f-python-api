# Installation

`python3 setup.py install`

or

`pip install git+https://codeberg.org/OPAS-F/opas-f-python-api.git`

# Usage
see examples directory or [OPAS-F homepage](https://opasf.github.io/docs/)

Issue-Tracker can be found [here](https://codeberg.org/OPAS-F/opas-f-python-api/issues)


# Documentation notes for later wiki edits

- how to write custom clients


# TODOS
- handle status codes and retry 3 times if failed
- handle expired refresh tokens
- handle pagination


## Not yet implemented

- signup
- status/check
- blog application
- master slave application
