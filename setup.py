from setuptools import setup

setup(
    name='opasf_api',
    version='0.1.2',
    packages=['opasf_api', 'opasf_api.core', 'opasf_api.clients', 'opasf_api.clients.accounts',
              'opasf_api.clients.bughunting', 'opasf_api.clients.pentesting'],
    url='https://codeberg.org/OPAS-F/opas-f-python-api',
    install_requires=['requests', 'pysocks'],
    license='GPLv3',
    author='user',
    author_email='cysec(at]riseup.net',
    description='Python API for Open Pentesting and Security Framework'
)
