import logging
import requests

class Client:
    def fetch_slave_queue(self):
        url = self.api_url + "/master-slave/slave-queue"
        resp = requests.get(url, headers=self._get_auth_header())
        if resp.status_code != 200:
            logging.info("error fetching slave queue")
        logging.debug(resp.json())
        return resp.json()

    def get_bugs(self, **kwargs):
        bugs = []
        endpoint = self._get_endpoint("/bughunting/bugs", **kwargs)
        while True:
            resp = self._do_get(endpoint)
            bugs += resp.get('results', [])
            if resp.get('next'):
                endpoint = resp['next']
            else:
                break
        return bugs

    def create_project(self, name, due_date=None, **kwargs):
        endpoint = self._get_endpoint("/pentesting/projects/", **kwargs)
        data = {'name': name}
        if due_date:
            data['due_date'] = str(due_date)
        return self._do_post(endpoint, data)

    def create_host(self, project_id, ip, command_id=None, owned=False, os=None, mac=None, status="up", **kwargs):
        endpoint = self._get_endpoint("/pentesting/hosts/", **kwargs)
        data = {
            'project': project_id, 'ip': ip, 'command': command_id,
            'owned': owned, 'mac': mac, 'status': status, 'os': os
        }
        return self._do_post(endpoint, data)

    def create_service(self, host_id, name, port, protocol, command_id=None, version=None, status=None, **kwargs):
        endpoint = self._get_endpoint("/pentesting/services/", **kwargs)
        data = {
            'host': host_id, 'name': name, 'port': port, 'protocol': protocol,
            'command': command_id, 'version': version, 'status': status
        }
        return self._do_post(endpoint, data)

    def create_vulnerability(self, service_id, name, severity, command_id=None, description=None,
                             approved=False, poc=None, **kwargs):
        endpoint = self._get_endpoint("/pentesting/vulnerabilities/", **kwargs)
        data = {
            'service': service_id, 'name': name, 'severity': severity, 'command': command_id,
            'description': description, 'approved': approved, 'poc': poc
        }
        return self._do_post(endpoint, data)

    def create_hostname(self, name, host_id, command_id=None, **kwargs):
        endpoint = self._get_endpoint("/pentesting/hostnames/", **kwargs)
        data = {'host': host_id, 'name': name, 'command': command_id}
        return self._do_post(endpoint, data)

    def create_web_vulnerability(self, service_id, name, severity, hostname_id, path, param=None,
                                 command_id=None, description=None, approved=False, poc=None,
                                 request=None, response=None, **kwargs):
        endpoint = self._get_endpoint("/pentesting/web-vulnerabilities/", **kwargs)
        data = {
            'name': name, 'severity': severity, 'service': service_id, 'hostname': hostname_id, 'path': path,
            'param': param, 'command': command_id, 'description': description, 'approved': approved, 'poc': poc,
            'request': request, 'response': response
        }
        return self._do_post(endpoint, data)

    def create_user_application(self, client_type, grant_type, name, redirect_uris=None,
                                skip_authorization=False, **kwargs):
        endpoint = self._get_endpoint("/master-slave/applications/", **kwargs)
        data = {'client_type': client_type, 'authorization_grant_type': grant_type,
                'name': name, 'redirect_uris': redirect_uris, 'skip_authorization': skip_authorization}
        return self._do_post(endpoint, data)

    def create_queue_item(self, project_id, command, start_date, in_progress=False, finished=False, **kwargs):
        endpoint = self._get_endpoint("/master-slave/queues/", **kwargs)
        data = {'project': project_id, 'command': command, 'start_date': start_date,
                'in_progress': in_progress, 'finished': finished}
        return self._do_post(endpoint, data)

    def queue_item_in_progress(self, item_id, **kwargs):
        endpoint = self._get_endpoint("/master-slave/slave-queue/%s/in-progress" % item_id, **kwargs)
        return self._do_update(endpoint, {'in_progress': True})

    def queue_item_finished(self, item_id, **kwargs):
        endpoint = self._get_endpoint("/master-slave/slave-queue/%s/finished" % item_id, **kwargs)
        return self._do_update(endpoint, {'finished': True})

    def command_started(self, project_id, command, **kwargs):
        endpoint = self._get_endpoint("/pentesting/commands/", **kwargs)
        data = {'project': project_id, 'command': command}
        return self._do_post(endpoint, data)

    def command_ended(self, command_id, finished_at, **kwargs):
        endpoint = self._get_endpoint("/pentesting/commands/%s/ended/" % command_id, **kwargs)
        data = {'finished_at': finished_at}
        return self._do_update(endpoint, data)

    def retrieve_profile(self, **kwargs):
        endpoint = self._get_endpoint("/accounts/profile/", **kwargs)
        return self._do_get(endpoint)

    def retrieve_user_profile(self, user_id, **kwargs):
        endpoint = self._get_endpoint("/accounts/%s/profile/" % user_id, **kwargs)
        return self._do_get(endpoint)

    def update_profile(self, bio=None, public_key=None, **kwargs):
        endpoint = self._get_endpoint("/accounts/profile/", **kwargs)
        data = {'bio': bio, 'public_key': public_key}
        return self._do_update(endpoint, data)

    def retrieve_account_settings(self, **kwargs):
        endpoint = self._get_endpoint("/accounts/settings/", **kwargs)
        return self._do_get(endpoint)

    def update_account_settings(self, bughunting_email_submit=False, public_profile=True, **kwargs):
        endpoint = self._get_endpoint("/accounts/settings/", **kwargs)
        data = {'bughunting_email_submit': bughunting_email_submit, 'public_profile': public_profile}
        return self._do_update(endpoint, data)

    def fetch_captcha(self, **kwargs):
        endpoint = self._get_endpoint("/accounts/signup/captcha", **kwargs)
        return self._do_unauth_post(endpoint, {})

    def signup(self, username, password, email=None, captcha_key=None, captcha_value=None,**kwargs):
        endpoint = self._get_endpoint("/accounts/signup", **kwargs)
        data = {'username': username, 'password': password}
        if email:
            data['email'] = email
        if captcha_key and captcha_value:
            data['captcha_key'] = captcha_key
            data['captcha_value'] = captcha_value
        return self._do_unauth_post(endpoint, data)

    def delete_account(self, **kwargs):
        endpoint = self._get_endpoint("/accounts/delete/", **kwargs)
        return self._do_delete(endpoint)

    def create_blog(self):
        pass

    def update_blog(self):
        pass

    def delete_blog(self, blog_id):
        pass

    def read_blog(self, slug):
        pass

    def create_post(self):
        pass

    def read_post(self):
        pass

    def update_post(self):
        pass

    def delete_post(self):
        pass

    def update_bug(self):
        pass

    def get_disclosed_bugs(self):
        pass

    def get_user_applications(self):
        pass

    def update_user_application(self):
        pass

    def update_queue_item(self):
        pass

    def delete_queue_item(self):
        pass

    # TODO: discuss if slave should only be allowed to create items, but not read?
    def create_credentials(self):
        pass

    def get_single_credentials(self):
        pass

    def update_credentials(self):
        pass

    def delete_credentials(self):
        pass

    def update_hostname(self):
        pass

    def get_single_hostname(self):
        pass

    def delete_hostname(self):
        pass

    def get_single_host(self):
        pass

    def update_host(self):
        pass

    def delete_host(self):
        pass

    def get_single_service(self):
        pass

    def update_service(self):
        pass

    def delete_service(self):
        pass

    def get_single_vulnerability(self):
        pass

    def update_vulnerability(self):
        pass

    def delete_vulnerability(self):
        pass

    def get_single_web_vulnerability(self):
        pass

    def update_web_vulnerability(self):
        pass

    def delete_web_vulnerability(self):
        pass

    def get_single_command(self):
        pass

    def create_report(self):
        pass

    def create_encrypted_report(self):
        pass

    def get_single_encrypted_report(self):
        pass

    def get_single_report(self):
        pass

    def delete_encrypted_report(self):
        pass

    def delete_report(self):
        pass

    def update_project(self):
        pass

    def delete_project(self):
        pass
