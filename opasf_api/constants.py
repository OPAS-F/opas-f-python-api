import os

OPASF_HOME = os.path.expanduser('~/.opasf')
OPASF_AUTH_FILE = os.path.join(OPASF_HOME, 'auth.json')
