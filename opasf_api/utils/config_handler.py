import json
import os


def create_config_by_env():
    config_file = os.path.join(os.path.expanduser("~"), '.opasf/config.json')
    config = None
    with open(config_file, "r") as read_config_file:
        config = json.load(read_config_file)
    if config:
        config['user']['api_host'] = os.environ['API_HOST']
        config['user']['is_slave'] = os.environ['IS_SLAVE']
        config['user']['slave_client_id'] = os.environ['SLAVE_CLIENT_ID']
        config['user']['slave_client_secret'] = os.environ['SLAVE_CLIENT_SECRET']
        config['mode'] = os.environ['CLIENT_MODE']
        with open(config_file, "w") as write_config_file:
            json.dump(config, write_config_file)
