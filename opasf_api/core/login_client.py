"""
Core client that handles login related stuff.

inherent from BaseRequestClient for allowing usage of api request
"""
import json
import os
from opasf_api.core import BaseRequestClient
from opasf_api.constants import OPASF_AUTH_FILE, OPASF_HOME


class CoreLoginClient(BaseRequestClient):
    auto_login = True
    api_host = "http://localhost:8000/api"
    auth_header = None
    auth_file_content = None

    def __init__(self, **kwargs):
        super(CoreLoginClient, self).__init__(**kwargs)
        if self.auto_login:
            self.login()

    def get_api_host(self):
        return self.api_host

    def login(self):
        """
        Read auth.json file. if access_token exist, read token, else login and create auth.json if not exists
        :return:
        """
        if not self.get_auth_header():
            self.logger.info("No auth headers found. performing log in!")
            self.perform_login()

    def get_auth_header(self):
        raise NotImplementedError("You need to implement get_auth_header() method!")

    def perform_login(self):
        raise NotImplementedError("You need to implement perform_login() method!")

    def get_auth_file_content(self):
        if not self.auth_file_content:
            self._read_auth_file()
        return self.auth_file_content

    @staticmethod
    def _create_auth_file():
        """ creating an auth file if non exists """
        if not os.path.exists(OPASF_HOME):
            os.makedirs(OPASF_HOME)
        with open(OPASF_AUTH_FILE, "w") as auth_file:
            json.dump({}, auth_file)

    def _update_auth_file(self, data):
        """ updating auth file with given json data """
        if not os.path.isfile(OPASF_AUTH_FILE):
            self._create_auth_file()
        with open(OPASF_AUTH_FILE, "w") as auth_file:
            json.dump(data, auth_file)

    def _read_auth_file(self):
        """ read auth.json file and return content """
        if not os.path.isfile(OPASF_AUTH_FILE):
            self._create_auth_file()
        with open(OPASF_AUTH_FILE, "r") as auth_file:
            content = auth_file.read()
            self.auth_file_content = json.loads(content)

    def do_get(self, url, *args, **kwargs):
        return super(CoreLoginClient, self).do_get(url, headers=self.get_auth_header())

    def do_post(self, url, data, *args, **kwargs):
        return super(CoreLoginClient, self).do_post(url, data, headers=self.get_auth_header(), *args, **kwargs)

    def do_delete(self, url, *args, **kwargs):
        return super(CoreLoginClient, self).do_delete(url, headers=self.get_auth_header())

    def do_patch(self, url, data, *args, **kwargs):
        return super(CoreLoginClient, self).do_patch(url, data, headers=self.get_auth_header())
