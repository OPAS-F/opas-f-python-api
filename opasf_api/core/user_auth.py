from opasf_api.core.login_client import CoreLoginClient
from opasf_api.exceptions import ImproperlyConfigured


class UserAuthClient(CoreLoginClient):
    client_id = None
    client_secret = None

    def get_client_id(self):
        if not self.client_id:
            raise ImproperlyConfigured("You need to give a client_id for login to work")
        return self.client_id

    def get_client_secret(self):
        if not self.client_secret:
            raise ImproperlyConfigured("You need to give a client_secret for login to work")
        return self.client_secret

    def get_auth_token(self):
        """ get auth token from auth file content """
        return self.get_auth_file_content().get('access_token')

    def get_auth_header(self):
        """ return authentication header if token exists """
        if self.get_auth_token():
            return {'Authorization': 'Bearer %s' % self.get_auth_token()}
        return None

    def invalidate_access_token(self):
        """ set acess_token to None if it has expired """
        self.logger.debug("invalidate access token")
        content = self.get_auth_file_content()
        content['access_token'] = None
        self._update_auth_file(content)

    def perform_login(self):
        """ perform login request for authenticating user """
        auth = (self.get_client_id(), self.get_client_secret())
        data = {'grant_type': 'client_credentials'}
        url = self.get_api_host() + "/accounts/o/token/"
        response = self.do_data_post(url, data, auth=auth)
        self._update_auth_file(response.json())

    def handle_response(self, response, *args, **kwargs):
        """ handle response for update expired tokens """
        if response.status_code == 403:
            if response.json() and response.json().get('code') == 'token_not_valid':
                self.invalidate_access_token()
        return response
