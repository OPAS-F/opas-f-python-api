from opasf_api.core.client import CoreClient
import requests


class BaseRequestClient(CoreClient):
    """ client that handles all the request stuff """
    proxies = {}

    def do_post(self, url, data, headers=None, auth=None):
        response = requests.post(url, json=data, headers=headers, auth=auth, proxies=self.proxies)
        return self.handle_response(response)

    def do_get(self, url, headers=None):
        response = requests.get(url, headers=headers, proxies=self.proxies)
        return response

    def do_patch(self, url, data, headers=None):
        response = requests.patch(url, json=data, headers=headers, proxies=self.proxies)
        return self.handle_response(response)

    def do_delete(self, url, headers=None):
        response = requests.delete(url, headers=headers, proxies=self.proxies)
        return self.handle_response(response)

    def handle_response(self, response, *args, **kwargs):
        """ if not implemented in parent class, nothing will happen """
        return response

    def do_data_post(self, url, data, headers=None, auth=None):
        response = requests.post(url, data=data, headers=headers,auth=auth, proxies=self.proxies)
        return self.handle_response(response)
