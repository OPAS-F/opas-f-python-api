import logging


class CoreClient(object):
    """ core client which is the parent of all clients """
    logger = logging.getLogger(__name__)

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)
