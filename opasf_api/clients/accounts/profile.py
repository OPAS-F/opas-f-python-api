from opasf_api.core.user_auth import UserAuthClient


class ProfileClient(UserAuthClient):
    """ handle everything profile related """

    def delete_account(self):
        url = self.get_api_host() + "/accounts/delete/"
        return self.do_delete(url)

    def read_profile(self):
        url = self.get_api_host() + "/accounts/profile/"
        return self.do_get(url)

    def update_profile(self, bio=None, public_key=None):
        url = self.get_api_host() + "/accounts/profile/"
        data = {'bio': bio, 'pulbic_key': public_key}
        return self.do_patch(url, data)

    def read_user_profile(self, user_id):
        url = self.get_api_host() + "/accounts/%s/profile/" % user_id
        return self.do_get(url)

    def read_settings(self):
        url = self.get_api_host() + "/accounts/settings/"
        return self.do_get(url)

    def update_settings(self, bughunting_email_submit=False, public_profile=True):
        url = self.get_api_host() + "/accounts/settings/"
        data = {'bughunting_email_submit': bughunting_email_submit, 'public_profile': public_profile}
        return self.do_patch(url, data)

    # def login(self):
    #   TODO: implement login here, since it fits better here
    #   pass

    def verify_token(self):
        # TODO: implement
        pass
