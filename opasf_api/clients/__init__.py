from opasf_api.clients.bughunting import BughuntingClient
from opasf_api.clients.pentesting import PentestingClient
from opasf_api.clients.accounts import ProfileClient


class OPASFClient(BughuntingClient, PentestingClient, ProfileClient):
    pass


class SlaveClient(BughuntingClient):
    pass