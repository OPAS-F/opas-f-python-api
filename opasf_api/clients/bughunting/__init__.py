from opasf_api.core.user_auth import UserAuthClient


class BughuntingClient(UserAuthClient):
    """ contains all methods needed for handling bughunting application endpoints """

    def create_bug(self, domain, name, path, parameter, poc, contact_email,
                   is_fixed=False, method="GET", description=None, is_draft=False, send_email_copy=False):
        """ create a new bug """
        url = self.get_api_host() + "/bughunting/user-bugs/"
        data = {
            'domain': domain, 'name': name, 'parameter': parameter, 'path': path,
            'proof_of_concept': poc, 'method': method, 'description': description,
            'bug_info': {'is_fixed': is_fixed, 'is_draft': is_draft, 'contact_email': contact_email,
                         'send_email_copy_user': send_email_copy}
        }
        return self.do_post(url, data).json()

    def list_bugs(self):
        """ list all bugs: TODO: pagination """
        url = self.get_api_host() + "/bughunting/bugs/"
        return self.do_get(url).json()

    def read_bug(self, bug_id):
        """ get details of a single bug """
        url = self.get_api_host() + "/bughunting/bugs/%s/" % bug_id
        return self.do_get(url).json()

    def read_bug_with_token(self, id, uidb64, token):
        raise NotImplementedError("not yet implemented! this endpoint does not need authentication")

    def resend_bug_notification_mail(self, bug_id):
        """ send a fresh notification mail to domain contact email address """
        url = self.get_api_host() + "/bughunting/bugs/%s/resend-notification" % bug_id
        return self.do_get(url).json()

    def increase_bug_disclosure_deadline(self, bug_id, days):
        """ increase the disclosure deadline by X days """
        url = self.get_api_host() + "/bughunting/bugs/%s/disclosure-deadline" % bug_id
        data = {'days': days}
        return self.do_patch(url, data).json()
