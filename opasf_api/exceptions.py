
class ImproperlyConfigured(Exception):
    pass


class ExpiredToken(Exception):
    pass