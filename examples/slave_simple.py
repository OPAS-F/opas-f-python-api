import logging
import time
from opasf_api import SlaveClient

logging.basicConfig(level=logging.DEBUG)


if __name__ == '__main__':
    cid = ""
    secret = ""
    username = ""
    password = ""

    # initialize without auto login
    slave1 = SlaveClient(auto_login=False)
    # slave.set_proxy("http": "socks5h://localhost:9050"})
    slave1.login(username=username, password=password, client_id=cid, client_secret=secret)

    # or like this
    # initialize without auto login
    slave1 = SlaveClient(username=username, password=password,
                         client_id=cid, client_secret=secret,
                         proxies={'http': 'socks5h://localhost:9050'})

    while True:
        queue_items = slave1.fetch_slave_queue()
        for item in queue_items['results']:
            logging.debug("Executing: %s", item.get('command'))
            # Note: you may want to execute your queue command yet
            # Keep in mind, that this could be exploited by manipulating the queue
            # currently no protection is implemented, this is why you need to implement
            # queue command exec on your own, like using the command line interface from our repo, sorry for now!

            # os.system(queue_item.get('command') ....

            # Something like this will happen in the plugins, if you used the cli version
            # host = slave1.create_host(1, "34.42.12.143")
            # service = slave1.create_service(host.get('id'), "https", 443, "tcp")
            # hostname = slave1.create_hostname("www.example.com", host.get('id'))
            # vuln = slave1.create_vulnerability(service.get('id'), "buffer overflow", "High")
            slave1.queue_item_in_progress(item.get('id'))
            time.sleep(3)
            slave1.queue_item_finished(item.get('id'))
        logging.debug("Sleeping...")
        time.sleep(30)

