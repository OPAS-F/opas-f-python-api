import logging
import sys
from opasf_api.clients import OPASFClient

logging.basicConfig(level=logging.DEBUG)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        logging.error("usage: python client.py username password")
        sys.exit(1)
    handler = OPASFClient(username=sys.argv[1], password=sys.argv[2])

    # get all my submitted bugs
    bugs = handler.list_bugs()
    print(len(bugs))

    # create a new bug. bug details getting disclosed after 30 days
    bug = handler.create_bug("http://example.com", "SQL-Injection", "page",
                             "/index.php", "some proof of concept", "admin@example.com")

    # resend the bug notification mail to the domain contact email
    handler.resend_bug_notification_mail(bug.get('uuid'))

    # add 3 more days to the disclose date
    handler.increase_bug_disclosure_deadline(bug.get('uuid'), 3)

    # fetch bug details from the server
    handler.read_bug(bug.get('uuid'))
